<?php

$apikey = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
$city = $_POST['city']; 
$url = "https://api.openweathermap.org/data/2.5/weather?q=$city&lang=fr&units=metric&APPID=$apikey";

$contents = file_get_contents($url);
$clima=json_decode($contents);

$country=$clima->sys->country;
$long=$clima->coord->lon;
$lat=$clima->coord->lat;
$weather=$clima->weather[0]->description;
$temp=$clima->main->temp;
$temp_max=$clima->main->temp_max;
$temp_min=$clima->main->temp_min;
$pressure=$clima->main->pressure;
$humidity=$clima->main->humidity;
$visibility=$clima->visibility;
$wind_speed=$clima->wind->speed;
$wind_deg=$clima->wind->deg;
$clouds=$clima->clouds->all;
$sunset=$clima->sys->sunset;
$sunrise=$clima->sys->sunrise;
$icon=$clima->weather[0]->icon.".png";
$today = date("d-m-Y H:i");
$cityname = $clima->name; 

$previsions ="https://api.openweathermap.org/data/2.5/forecast?q=$city,$country&lang=fr&units=metric&&appid=$apikey";

$contents2 = file_get_contents($previsions);
$prev=json_decode($contents2,true);

if ($long == "")
{
    header('Location: index.php?str=La ville demandée n\'existe pas. Vérifiez l\'orthographe et recommencez.');
    exit();
}
  else 
  {
      require 'prog.php';
  }

?>