<!DOCTYPE html>
<html>
    <head>
        <title>myweather.</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href="style.css">
        <link href="https://fonts.googleapis.com/css?family=Righteous" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/balloon-css/0.5.0/balloon.min.css">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>

        <body> 
<div class="bg">
            <p class="title">myweather<span class="orange" data-balloon="developped for fun by twitter@groussel &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; datas based on openweathermap.org" data-balloon-pos="down" data-balloon-length="medium">.</span></p><br><br>
            <div class="width50 index">
                <?php
                $alert = $_GET['str'];
                if ($alert == "")
                    {
                    echo '';
                    }
                        elseif ($alert != 'La ville demandée n\'existe pas. Vérifiez l\'orthographe et recommencez.')
                        {
                            echo '';
                        }
                            else 
                                {
                                echo '
                                <div class="alert alert-danger" role="alert">
                                '.$alert.'
                                </div>
                                ';
                                }
                ?>
                
<form method="post" action="weather.php"> 
    
    <div class="form-group">
    <input type="text" required class="form-control" id="city" name="city" placeholder="De quelle ville souhaitez-vous connaitre la météo ?">
    </div>
    

<input type="submit" value="here we go!" class="btn btn-outline-warning btn-lg"> 
</form>
    
                </div>
</div>    
        </body>

</html>