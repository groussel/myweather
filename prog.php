<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href="style.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta charset="utf-8">
        <title>myweather. <?php print $city; ?></title>
    </head>
  

    
    <body>
<div class="bgview">
        <div class="width50">
            <h1 class="display-3">à <span class="text-warning"><strong><?php print $city; ?></strong></span> aujourd'hui</h1><br><p class="h6"> <?php print $today; ?> GMT</p>
            <div class="alert alert-success" width="300px">
                <img src='https://openweathermap.org/img/w/<?php print $icon; ?>'/ ><br>
                <p class="h3"><strong><?php print $weather; ?></strong><br><?php print $temp; ?> °C</p>
            </div>
    
<!--DEBUT BOUTON MODAL            -->
<button type="button" class="btn btn-primary btn-warning" data-toggle="modal" data-target="#exampleModal">
  voir les prévisions à 5 jours 
</button>
<br><br>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Prévisions à 5 jours</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <?php 
            foreach($prev as $ligne)
                {
	               var_dump($ligne);
                }
          ?>
<!--          <?php var_dump($prev); ?>-->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
      </div>
    </div>
  </div>
</div>

<!--FIN BOUTON MODAL            -->
        
            <div class="table-responsive">
            <table class="table table-striped table-hover table-condensed">
              <tbody>
                <tr>
                  <th scope="row">Lattitude</th>
                  <td><?php print $lat; ?> °</td>
                </tr>
                <tr>
                  <th scope="row">Longitude</th>
                  <td><?php print $long; ?> °</td>
                </tr>
                <tr>
                  <th scope="row">Température actuelle</th>
                  <td><?php print $temp; ?> °C</td>
                </tr>
                <tr>
                  <th scope="row">Température min.</th>
                  <td><?php print $temp_min; ?> °C</td>
                </tr>
                <tr>
                  <th scope="row">Température max.</th>
                  <td><?php print $temp_max; ?> °C</td>
                </tr>
                <tr>
                  <th scope="row">Pression</th>
                  <td><?php print $pressure; ?> hPa</td>
                </tr>
                <tr>
                  <th scope="row">Humidité</th>
                  <td><?php print $humidity; ?> %</td>
                </tr>
                <tr>
                  <th scope="row">Visibilité</th>
                  <td><?php print $visibility; ?> m</td>
                </tr>
                <tr>
                  <th scope="row">Vitesse du vent</th>
                  <td><?php print $wind_speed; ?> m/s</td>
                </tr>
                <tr>
                  <th scope="row">Direction du vent</th>
                  <td><?php print $wind_deg; ?> °</td>
                </tr>
                  <tr>
                  <th scope="row">Nuages</th>
                  <td><?php print $clouds; ?> %</td>
                </tr>
                <tr>
                  <th scope="row">Lever du soleil</th>
                  <td><?php echo gmdate("H:i:s", $sunrise); ?> GMT</td></tr>
                <tr>
                  <th scope="row">Coucher du soleil</th>
                  <td><?php echo gmdate("H:i:s", $sunset); ?> GMT</td>
                </tr>
                <tr>
                  <th scope="row">Pays</th>
                  <td><?php print $country; ?></td>
                </tr>
              </tbody>
            </table>
            </div>

<form method="post" action="weather.php"> 
    
    <div class="form-group">
    <input type="text" class="form-control inputGroup-sizing-sm" id="city" name="city" placeholder="Une autre ville ?"> 
    </div>
    
<input type="submit" value="here we go!" class="btn btn-outline-warning btn-sm"> 
<br><br>
</form>
            
        </div>
</div>        
<script>
$(document).ready(function(){
    $("#myBtn").click(function(){
        $("#myModal").modal();
    });
});
</script>



    </body>
</html>